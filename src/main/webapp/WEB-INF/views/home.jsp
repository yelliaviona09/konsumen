<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Halaman Utama</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        h1 {
            color: #333;
        }

        p {
            margin-bottom: 10px;
        }

        a {
            color: #007bff;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
<h1>Selamat Datang di Aplikasi Konsumen</h1>
<p>Ini adalah halaman utama aplikasi.</p>
<p>Anda dapat mengakses daftar konsumen dengan mengeklik <a href="/mvc/customers/all">di sini</a>.</p>
</body>
</html>
