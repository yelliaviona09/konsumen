<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Search Customers</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        h1, h2 {
            color: #333;
        }

        label {
            display: block;
            margin-top: 10px;
        }

        input[type="text"] {
            width: 300px;
            padding: 5px;
            border: 1px solid #ccc;
        }

        input[type="submit"] {
            padding: 10px 20px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            cursor: pointer;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
<h1>Search Customers</h1>
<form action="/mvc/customers/search" method="get">
    <label for="nama">Nama:</label>
    <input type="text" id="nama" name="nama"><br>

    <label for="status">Status:</label>
    <input type="text" id="status" name="status"><br>

    <input type="submit" value="Search">
</form>

<h2>Search Results</h2>
<table>
    <tr>
        <th>Nama</th>
        <th>Status</th>
    </tr>
    <tr th:each="customer : ${customers}">
        <td th:text="${customer.nama}"></td>
        <td th:text="${customer.status}"></td>
    </tr>
</table>
</body>
</html>
