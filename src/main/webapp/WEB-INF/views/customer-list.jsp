<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Customer List</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        h1 {
            color: #333;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
<h1>Customer List</h1>
<table>
    <tr>
        <th>Nama</th>
        <th>Status</th>
    </tr>
    <tr th:each="customer : ${customers}">
        <td th:text="${customer.nama}"></td>
        <td th:text="${customer.status}"></td>
    </tr>
</table>
</body>
</html>
