package com.example.demo.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.time.ZonedDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "konsumen")
public class Konsumen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama")
    private String nama;

    @Column(name = "alamat")
    private String alamat;


    @Column(name= "kota")
    private String kota;

    @Column(name = "provinsi")
    private String provinsi;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private ZonedDateTime tgl_registrasi;

    @Column(name = "status")
    private String status;

}