package com.example.demo.repo;
import com.example.demo.entity.Konsumen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KonsumenRepository extends JpaRepository<Konsumen, Long> {
    @Query("SELECT k FROM Konsumen k WHERE (:nama IS NULL OR k.nama ilike concat('%', :nama,'%')) OR (:status IS NULL OR k.status = :status)")
    List<Konsumen> findByNamaContainingIgnoreCaseAndStatus(@Param("nama") String nama, @Param("status") String status);

    List<Konsumen>findByNamaContainingIgnoreCase(String nama);
    List<Konsumen>findByStatus(String status);
}
