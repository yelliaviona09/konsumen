package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoForSaltApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoForSaltApplication.class, args);
	}

}
