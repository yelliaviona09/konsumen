package com.example.demo.service;

import com.example.demo.entity.Konsumen;
import com.example.demo.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.repo.KonsumenRepository;

import java.util.List;

@Service
public class KonsumenService {

    private KonsumenRepository konsumenRepository;
    @Autowired
    public KonsumenService(KonsumenRepository konsumenRepository) {
        this.konsumenRepository = konsumenRepository;
    }

    public Konsumen createCustomer(Konsumen customer) {
        return konsumenRepository.save(customer);
    }

    public List<Konsumen> getAllCustomers() {
        List<Konsumen> customers = konsumenRepository.findAll();
        if (customers.isEmpty()) {
            throw new ResourceNotFoundException("empty consumer data");
        }
        return customers;
    }

    public List<Konsumen> searchCustomers(String nama, String status) {
        return konsumenRepository.findByNamaContainingIgnoreCaseAndStatus(nama, status);

    }
}
