//package com.example.demo.controller;
//
//import jakarta.servlet.http.HttpServletRequest;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.boot.web.servlet.error.ErrorController;
//import org.springframework.web.bind.annotation.Controller;
//@Controller
//public class ErrorController implements org.springframework.boot.web.servlet.error.ErrorController {
//
//    @RequestMapping("/error")
//    public String handleError(HttpServletRequest request) {
//        // Logika untuk menangani kesalahan
//        return "error"; // Mengembalikan nama tampilan "error"
//    }
//
//    @Override
//    public String getErrorPath() {
//        return "/error";
//    }
//}
//
