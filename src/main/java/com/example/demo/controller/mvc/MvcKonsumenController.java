package com.example.demo.controller.mvc;

import com.example.demo.entity.Konsumen;
import com.example.demo.service.KonsumenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/mvc/customers")
public class MvcKonsumenController {
    @Autowired
    private KonsumenService konsumenService;

    @GetMapping("/")
    public String homePage() {
        return "home";
    }

    @PostMapping("/add")
    public String createCustomer(@ModelAttribute Konsumen customer) {
        konsumenService.createCustomer(customer);
        return "redirect:/mvc/customers/all";
    }

    @GetMapping("/all")
    public String getAllCustomers(Model model) {
        List<Konsumen> customers = konsumenService.getAllCustomers();
        model.addAttribute("customers", customers);
        return "customer-list"; // Mengembalikan nama tampilan "customer-list.html"
    }

    @GetMapping("/search")
    public String searchCustomers(@RequestParam(required = false) String nama,
                                  @RequestParam(required = false) String status,
                                  Model model) {
        List<Konsumen> customers = konsumenService.searchCustomers(nama, status);
        model.addAttribute("customers", customers);
        return "customer-search";
    }
}
