package com.example.demo.controller;



import com.example.demo.entity.Konsumen;
import com.example.demo.service.KonsumenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class KonsumenController {
    @Autowired
    private KonsumenService konsumenService;

    @PostMapping("/add")
    public Konsumen createCustomer(@RequestBody Konsumen konsumen) {
        return konsumenService.createCustomer(konsumen);
    }

    @GetMapping("/all")
    public List<Konsumen> getAllCustomers() {

        return konsumenService.getAllCustomers();
    }

    @GetMapping("/search")
    public List<Konsumen> searchCustomers(@RequestParam(required = false) String nama,
                                          @RequestParam(required = false) String status) {
        return konsumenService.searchCustomers(nama, status);
    }
}

