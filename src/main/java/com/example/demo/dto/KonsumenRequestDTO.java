package com.example.demo.dto;
import com.example.demo.entity.Konsumen;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class KonsumenRequestDTO {

    private String nama;

    private String alamat;

    private String kota;

    private String provinsi;

    private String status;

    public Konsumen convertTOEntity(){
        return Konsumen.builder().nama(this.nama)
                .alamat(this.alamat)
                .kota(this.kota)
                .provinsi(this.provinsi)
                .status(this.status).build();
    }
}
